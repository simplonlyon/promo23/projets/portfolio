# Projet Portfolio

Faire votre portfolio pour le site simplonlyon.fr dans lequel devront figurés :
* Une petite présentation
* La liste de vos projets
* Un contact
* Le lien vers votre gitlab
* Eventuellement vos préférences en terme de langages/techno

Le site devra être responsive et valide W3C. Il est fortement conseillé d'utiliser la grid et les components bootstraps.

Valider également l'accessibilité du site avec une extension comme WAVE.

## Conception
Le projet devra commencer par avoir : 
* La ou les maquettes fonctionnelles du site (qui doivent en gros indiquée les éléments d'interface et leur positionnements)
* Un projet gitlab avec un README qui décrit le projet puis qui contiendra à terme vos maquettes
* Faire la structure HTML complète de l'application avant de commencer le style

## Mise en ligne

1. Faire en sorte de mettre le site en ligne en utilisant les Gitlab Pages.
2. Mettre le lien de votre Gitlab Pages dans votre profil sur simplonlyon.fr
